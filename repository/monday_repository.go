package repository

import (
	"context"
	"fmt"
	"log"

	"echo-doktuz-middleware/models"
	"echo-doktuz-middleware/services/monday"

	"github.com/machinebox/graphql"
)

const (
	timeFormat = "2006-01-02T15:04:05.999Z07:00" // reduce precision from RFC3339Nano as date format
)

type mondayRepository struct {
	Conn *graphql.Client
}

func NewMondayRepository(Conn *graphql.Client) monday.Repository {
	return &mondayRepository{Conn}
}

const token = "eyJhbGciOiJIUzI1NiJ9.eyJ0aWQiOjEwMTM4NjE2NiwidWlkIjoxNjE3MTk1NywiaWFkIjoiMjAyMS0wMy0wMVQxNjo1MTozNy4zMDhaIiwicGVyIjoibWU6d3JpdGUiLCJhY3RpZCI6NzEyNjgzMCwicmduIjoidXNlMSJ9.8b0CNLJr6CMAkXVs06qnrh_5begAugTdwHr7iHKRFIs"

// RunRequest executes request and decodes response into response parm (address of object)
func RunRequest(client *graphql.Client, req *graphql.Request, response interface{}) error {
	req.Header.Set("Cache-Control", "no-cache")
	req.Header.Set("Authorization", token)
	req.Header.Set("Content-Type", "application/json")
	ctx := context.Background()
	err := client.Run(ctx, req, response)
	return err
}

// GetUsers returns []User for all users.
func (m *mondayRepository) GetAllUsers(ctx context.Context) ([]*models.User, error) {
	req := graphql.NewRequest(`
	    query {
            users {
                id name email
            }
        }
    `)
	var response struct {
		Users []*models.User `json:"users"`
	}
	err := RunRequest(m.Conn, req, &response)
	return response.Users, err
}

// GetColumns returns []Column for specified boardId.
func (m *mondayRepository) GetAllColumns(ctx context.Context, boardId int) ([]*models.Column, error) {
	req := graphql.NewRequest(`
	    query ($boardId: [Int]) {
            boards (ids: $boardId) {
                columns {id title type settings_str}
            }
        }
    `)
	req.Var("boardId", []int{boardId})
	type board struct {
		Columns []*models.Column `json:"columns"`
	}
	var response struct {
		Boards []*board `json:"boards"`
	}
	err := RunRequest(m.Conn, req, &response)
	return response.Boards[0].Columns, err
}

// GetBoards returns []Board for all boards.
func (m *mondayRepository) GetAllBoards(ctx context.Context) ([]*models.Board, error) {
	req := graphql.NewRequest(`
	    query {
            boards {
                id name
            }
        }
    `)
	var response struct {
		Boards []*models.Board `json:"boards"`
	}
	err := RunRequest(m.Conn, req, &response)
	return response.Boards, err
}

func (m *mondayRepository) GetColumnValue(ctx context.Context, itemId int, columnId string) (string, error) {
	req := graphql.NewRequest(`
	query($itemId: [Int], $columnId: [String]) {
        items (ids: $itemId) {
          column_values(ids:$columnId) {
            value
          }
        }
      }
	`)
	req.Var("itemId", itemId)
	req.Var("columnId", columnId)

	var response map[string]interface{}
	err := RunRequest(m.Conn, req, &response)
	if err != nil {
		log.Println(err)
		return "", err
	}
	items := response["items"].([]interface{})
	i := items[0].(map[string]interface{})
	j := i["column_values"].([]interface{})
	value := ((j[0].(map[string]interface{}))["value"]).(string)
	return value, nil
}

func (m *mondayRepository) ChangeColumnValue(ctx context.Context, boardId int, itemId int, columnId string, value string) error {
	req := graphql.NewRequest(`
	mutation change_column_value($boardId: Int!, $itemId: Int!, $columnId: String!, $value: JSON!) {
        change_column_value(board_id: $boardId, item_id: $itemId, column_id: $columnId, value: $value) {
          id
        }
      }
	`)

	req.Var("boardId", boardId)
	req.Var("itemId", itemId)
	req.Var("columnId", columnId)
	req.Var("value", value)

	var response interface{}
	err := RunRequest(m.Conn, req, &response)
	if err != nil {
		return err
	}
	return nil
}

func (m *mondayRepository) AddTrigger(ctx context.Context, modMonday *models.Monday) error {
	fmt.Println(modMonday, "- Add")
	return nil
}

func (m *mondayRepository) UpdateTrigger(ctx context.Context, modMonday *models.Monday) error {
	fmt.Println(modMonday, "- Update")
	return nil
}

func (m *mondayRepository) DeleteTrigger(ctx context.Context, itemId int) error {
	fmt.Println(itemId, "- Delete")
	req := graphql.NewRequest(`
	mutation delete_item($itemId: Int!){
		delete_item(item_id: $itemId) {
		  id
		}
	  }
	`)
	req.Var("itemId", itemId)

	var response interface{}
	err := RunRequest(m.Conn, req, &response)
	if err != nil {
		return err
	}

	return nil
}
