package config

import (
	"fmt"

	"github.com/spf13/viper"
)

func SetAppEnvironment() {
	viper.SetConfigName("config")
	viper.SetConfigType("json")
	viper.AddConfigPath(".")
	err := viper.ReadInConfig()
	if err != nil {
		fmt.Println(err)
		panic(err)
	}
}
