package monday

import (
	"context"
	"echo-doktuz-middleware/models"
)

// Usecase represent the article's usecases
type Usecase interface {
	GetAllUsers(ctx context.Context) ([]*models.User, error)
	GetAllBoards(ctx context.Context) ([]*models.Board, error)
	GetAllColumns(ctx context.Context, id int) ([]*models.Column, error)
	ChangeColumnValue(ctx context.Context, modMonday *models.Monday) error
	AddTrigger(ctx context.Context, modMonday *models.Monday) error
	UpdateTrigger(ctx context.Context, modMonday *models.Monday) error
	DeleteTrigger(ctx context.Context, modMonday *models.Monday) error
}
