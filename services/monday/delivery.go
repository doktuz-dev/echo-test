package monday

import (
	"echo-doktuz-middleware/database"
	"echo-doktuz-middleware/middlewares/cookies"
	"echo-doktuz-middleware/middlewares/errorHandler"
	"echo-doktuz-middleware/middlewares/security"
	"echo-doktuz-middleware/middlewares/sessions"
	"echo-doktuz-middleware/middlewares/tokens"
	"echo-doktuz-middleware/models"
	"encoding/json"
	"fmt"
	"strconv"
	"time"

	"github.com/labstack/echo/v4"
)

type MondayHandler struct {
	MUsecase Usecase
}

func NewMondayHandler(r *echo.Group, us Usecase) {
	handler := &MondayHandler{
		MUsecase: us,
	}
	var SessionsCollection database.SessionsCollection = database.SessionsCollection{C: database.DokrDB.Collection("sessions")}
	r.POST("/login", login)
	r.GET("/users", handler.GetAllUsers, security.CheckSecurity(&SessionsCollection))
	r.GET("/boards", handler.GetAllBoards)
	r.GET("/columns/:id", handler.GetAllColumns)
	r.POST("/changes", handler.ChangeColumnValue)
	r.POST("/trigger/add", handler.AddTrigger)
	r.POST("/trigger/update", handler.UpdateTrigger)
	r.POST("/trigger/delete", handler.DeleteTrigger)

}

// @Summary Monday login
// @Tags monday
// @Description Monday login
// @Param   monday     body    object     true        "Monday Body"     models.LoginUser
// @Accept  json
// @Produce  json
// @Success 200 {object} models.Session
// @Router /monday/login [post]
func login(ctx echo.Context) error {
	var user models.LoginUser
	var SessionsCollection database.SessionsCollection = database.SessionsCollection{C: database.DokrDB.Collection("sessions")}
	var LoginCollection database.LoginCollection = database.LoginCollection{C: database.DokrDB.Collection("users")}

	err := json.NewDecoder(ctx.Request().Body).Decode(&user)
	if err != nil {
		fmt.Println("Login", err)
		return err
	}
	res := LoginCollection.ValidateUser(nil, user)
	if res {
		ssid := sessions.CreateSSID()
		jwtString, jwtErr := tokens.CreateJwtString(ssid)
		if jwtErr != nil {
			json := errorHandler.CreateJsonError(jwtErr)
			return ctx.JSON(echo.ErrUnauthorized.Code, json)
		}
		session := sessions.NewSessionObject(ssid, user.Username, "MIDDLEWARE")
		err = SessionsCollection.AddSession(nil, session)
		if err != nil {
			json := errorHandler.CreateJsonError(err)
			return ctx.JSON(echo.ErrUnauthorized.Code, json)
		}
		cookie := cookies.CreateCookie("ApiTokenV1", jwtString, time.Now().Add(time.Minute*30))
		ctx.SetCookie(cookie)
		return ctx.JSON(200, session)
	} else {
		var json map[string]interface{}
		json["error"] = "Forbidden. User does not exist."
		return ctx.JSON(echo.ErrForbidden.Code, json)
	}
}

// @Summary Monday users
// @Tags monday
// @Description Monday users
// @Accept  json
// @Produce  json
// @Success 200 {array} models.User
// @Router /monday/users [get]
func (m *MondayHandler) GetAllUsers(ctx echo.Context) error {
	fmt.Println("Get All Users middleware")
	c := ctx.Request().Context()

	users, err := m.MUsecase.GetAllUsers(c)
	if err != nil {
		errorHandler.HandleError(ctx, err)
		return err
	}
	return ctx.JSON(200, users)
}

// @Summary Monday boards
// @Tags monday
// @Description Monday boards
// @Accept  json
// @Produce  json
// @Success 200 {array} models.Board
// @Router /monday/boards [get]
func (m *MondayHandler) GetAllBoards(ctx echo.Context) error {

	c := ctx.Request().Context()
	boards, err := m.MUsecase.GetAllBoards(c)
	if err != nil {
		errorHandler.HandleError(ctx, err)
		return err
	}

	return ctx.JSON(200, boards)

}

// @Summary Monday columns
// @Tags monday
// @Description Monday columns
// @Param   id     path    string     true        "Board ID"
// @Accept  json
// @Produce  json
// @Success 200 {array} models.Column
// @Router /monday/columns/{id} [get]
func (m *MondayHandler) GetAllColumns(ctx echo.Context) error {

	id := ctx.Param("id")
	idInt, err := strconv.Atoi(id)
	if err != nil {
		errorHandler.HandleError(ctx, err)
		return err
	}
	c := ctx.Request().Context()
	colums, erro := m.MUsecase.GetAllColumns(c, idInt)

	if erro != nil {
		errorHandler.HandleError(ctx, err)
		return erro
	}
	return ctx.JSON(200, colums)

}

// @Summary Monday changes
// @Tags monday
// @Description Monday changes
// @Param   monday     body    object     true        "Monday Body"     models.Monday
// @Accept  json
// @Produce  json
// @Success 201 {object} models.ResponseSuccess
// @Router /monday/changes [post]
func (m *MondayHandler) ChangeColumnValue(ctx echo.Context) error {
	var monday models.Monday
	err := json.NewDecoder(ctx.Request().Body).Decode(&monday)
	if err != nil {
		ctx.JSON(errorHandler.ErrUnprocessableEntity.Code, err.Error())
		return err
	}
	c := ctx.Request().Context()

	err = m.MUsecase.ChangeColumnValue(c, &monday)
	if err != nil {
		errorHandler.HandleError(ctx, err)
		return err
	}
	return ctx.JSON(200, models.ResponseSuccess{Message: "success"})

}

// @Summary Monday trigger add
// @Tags monday
// @Description Monday changes trigger
// @Param   monday     body    object     true        "Monday Body"     models.Monday
// @Accept  json
// @Produce  json
// @Success 201 {object} models.ResponseSuccess
// @Router /monday/trigger/add [post]
func (m *MondayHandler) AddTrigger(ctx echo.Context) error {
	var monday models.Monday
	err := json.NewDecoder(ctx.Request().Body).Decode(&monday)
	if err != nil {
		return ctx.JSON(errorHandler.ErrUnprocessableEntity.Code, err.Error())
	}
	c := ctx.Request().Context()

	err = m.MUsecase.AddTrigger(c, &monday)
	if err != nil {
		errorHandler.HandleError(ctx, err)
		return err
	}
	return ctx.JSON(200, monday)

}

// @Summary Monday trigger update
// @Tags monday
// @Description Monday changes trigger
// @Param   monday     body    object     true        "Monday Body"     models.Monday
// @Accept  json
// @Produce  json
// @Success 201 {object} models.ResponseSuccess
// @Router /monday/trigger/update [post]
func (m *MondayHandler) UpdateTrigger(ctx echo.Context) error {
	var monday models.Monday
	err := json.NewDecoder(ctx.Request().Body).Decode(&monday)
	if err != nil {
		return ctx.JSON(errorHandler.ErrUnprocessableEntity.Code, err.Error())
	}
	c := ctx.Request().Context()

	err = m.MUsecase.UpdateTrigger(c, &monday)
	if err != nil {
		errorHandler.HandleError(ctx, err)
		return err
	}
	return ctx.JSON(200, monday)
}

// @Summary Monday trigger delete
// @Tags monday
// @Description Monday changes trigger
// @Param   monday     body    object     true        "Monday Body"     models.Monday
// @Accept  json
// @Produce  json
// @Success 201 {object} models.ResponseSuccess
// @Router /monday/trigger/delete [post]
func (m *MondayHandler) DeleteTrigger(ctx echo.Context) error {
	var monday models.Monday
	err := json.NewDecoder(ctx.Request().Body).Decode(&monday)
	if err != nil {

		return ctx.JSON(errorHandler.ErrUnprocessableEntity.Code, err.Error())
	}
	c := ctx.Request().Context()

	err = m.MUsecase.DeleteTrigger(c, &monday)
	if err != nil {
		errorHandler.HandleError(ctx, err)
		return err
	}
	return ctx.JSON(200, monday)
}
