package monday

import (
	"context"
	"echo-doktuz-middleware/models"
)

type Repository interface {
	GetAllUsers(ctx context.Context) ([]*models.User, error)
	GetAllBoards(ctx context.Context) ([]*models.Board, error)
	GetAllColumns(ctx context.Context, id int) ([]*models.Column, error)
	GetColumnValue(ctx context.Context, itemId int, columnId string) (string, error)
	ChangeColumnValue(ctx context.Context, boardId int, itemId int, columnId string, value string) error
	AddTrigger(ctx context.Context, modMonday *models.Monday) error
	UpdateTrigger(ctx context.Context, modMonday *models.Monday) error
	DeleteTrigger(ctx context.Context, itemId int) error
}
