package tokens

import (
	"fmt"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/spf13/viper"
)

func CreateJwtString(ssid string) (string, error) {
	atClaims := jwt.MapClaims{}
	atClaims["ssid"] = ssid
	atClaims["exp"] = time.Now().Add(time.Second * 20).Unix()
	at := jwt.NewWithClaims(jwt.SigningMethodHS256, atClaims)
	token, err := at.SignedString([]byte(viper.GetString("secret")))
	if err != nil {
		return "", err
	}
	return token, nil
}

func RefreshJwtString(ssid string) (string, error) {
	jwt, err := CreateJwtString(ssid)
	return jwt, err
}

// This function returns a *jwt.Token parsed from a tokenString.
func ParseJwtString(tokenString string) (*jwt.Token, error) {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(viper.GetString("secret")), nil
	})
	if err != nil {
		return token, err
	}
	return token, nil
}

func ExtractSSIDfromToken(token *jwt.Token) string {

	claims := token.Claims.(jwt.MapClaims)
	ssid := claims["ssid"]
	return ssid.(string)

}
