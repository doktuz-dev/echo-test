package errorHandler

import (
	"echo-doktuz-middleware/models"

	"github.com/labstack/echo/v4"
)

type ResponseError struct {
	Message string `json:"message"`
}

func CreateJsonError(err error) map[string]interface{} {
	var json map[string]interface{}
	json["error"] = err.Error()
	return json
}

func HandleError(ctx echo.Context, err error) {
	if err == nil {
		return
	}

	statusCode := echo.ErrInternalServerError.Code
	switch err {
	case models.ErrInternalServerError:
		statusCode = echo.ErrInternalServerError.Code
	case models.ErrNotFound:
		statusCode = echo.ErrNotFound.Code
	case models.ErrConflict:
		statusCode = echo.ErrTooManyRequests.Code
	}
	ctx.Logger().Error(err)
	ctx.JSON(statusCode, ResponseError{Message: err.Error()})

}

var ErrUnprocessableEntity *echo.HTTPError = echo.NewHTTPError(422, ResponseError{Message: "Unprocessable entity"})
