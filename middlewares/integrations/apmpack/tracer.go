package apmpack

import (
	"go.elastic.co/apm"
	"go.elastic.co/apm/transport"
)

type ApmTracer struct {
	Config      *ApmConfig
	Transporter transport.Transport
}

func (t *ApmTracer) GetCustomTracer() *apm.Tracer {
	tracer, err := apm.NewTracerOptions(apm.TracerOptions{
		ServiceName:        t.Config.ServiceName,
		ServiceVersion:     t.Config.ServiceVersion,
		ServiceEnvironment: t.Config.Environment,
		Transport:          t.Transporter,
	})
	if err != nil {
		panic(err)
	}
	return tracer
}
