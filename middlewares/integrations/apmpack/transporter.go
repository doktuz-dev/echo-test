package apmpack

import (
	"net/url"

	"go.elastic.co/apm/transport"
)

func CreateTransporter(t *ApmConfig) transport.Transport {
	transporter, err := transport.NewHTTPTransport()
	if err != nil {
		panic(err)
	}
	uriServer, err := url.Parse(t.ServerUrl)
	if err != nil {
		panic(err)
	}
	transporter.SetSecretToken(t.SecretToken)
	transporter.SetServerURL(uriServer)
	return transporter
}
