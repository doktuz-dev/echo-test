package apmpack

import "github.com/spf13/viper"

type ApmConfig struct {
	ServiceName    string
	ServerUrl      string
	SecretToken    string
	Environment    string
	ServiceVersion string
}

func (c *ApmConfig) GetApmConfig() {
	c.ServiceName = viper.GetString("apm.serviceName")
	c.ServerUrl = viper.GetString("apm.serverUrl")
	c.SecretToken = viper.GetString("apm.secretToken")
	c.Environment = viper.GetString("apm.environment")
	c.ServiceVersion = viper.GetString("apm.serviceVersion")
}
