package sessions

import (
	"echo-doktuz-middleware/models"
	"time"

	"github.com/gorilla/securecookie"
	"github.com/thanhpk/randstr"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func NewSessionObject(SSID, username, app string) *models.Session {
	newSession := &models.Session{
		ID:              primitive.NewObjectID().Hex(),
		Ssid:            SSID,
		Username:        username,
		Enabled:         true,
		StartDate:       time.Now().Unix(),
		EndDate:         time.Now().Add(1 * time.Hour).Unix(),
		App:             app,
		CreatedDate:     time.Now().Unix(),
		LastUpdatedDate: time.Now().Unix(),
	}
	return newSession
}

// This function creates a binary secure cookie.
func generateSecureCookie() *securecookie.SecureCookie {
	hashKey := securecookie.GenerateRandomKey(64)
	blockKey := securecookie.GenerateRandomKey(32)
	s := securecookie.New(hashKey, blockKey)
	return s
}

func generateRandomSSID(name string, s *securecookie.SecureCookie) string {
	cookieString, err := s.Encode(name, randstr.String(20))
	if err == nil {
		return cookieString
	}
	return ""
}

func CreateSSID() string {
	bin := generateSecureCookie()
	sid := generateRandomSSID("SSID", bin)
	return sid
}
