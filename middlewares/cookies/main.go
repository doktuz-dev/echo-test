package cookies

import (
	"net/http"
	"time"
)

func CreateCookie(name, value string, exp time.Time) *http.Cookie {
	cookie := &http.Cookie{
		Name:     name,
		Value:    value,
		Secure:   true,
		HttpOnly: true,
		Path:     "/",
		Expires:  exp,
		SameSite: http.SameSiteLaxMode,
	}
	return cookie
}
