package logsHandler

import (
	"fmt"
	"log"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	rotatelogs "github.com/lestrrat-go/file-rotatelogs"
)

func RequestsLogs() middleware.BodyDumpHandler {
	return func(c echo.Context, reqBody, resBody []byte) {
		fmt.Printf("%s", resBody)
		date := time.Now()
		method := c.Request().Method
		uri := c.Request().RequestURI
		status := c.Request().Response.StatusCode
		reqHeaders := c.Request().Header
		resHeaders := c.Response().Header()
		respBody := fmt.Sprintf("%s", resBody)
		pathToLog := "logs/log%Y%m%d.log"
		w, err := rotatelogs.New(
			pathToLog,
			rotatelogs.WithMaxAge(time.Hour*24*5),
			rotatelogs.WithRotationTime(time.Minute*2))
		if err != nil {
			panic(err)
		}
		log.SetOutput(w)
		log.SetFlags(0)
		log.Println("----------- New Request -----------------")
		log.Printf("Date: %v - Method: %v - Uri: %v - Status: %v", date, method, uri, status)

		log.Println("----------- Request Headers -----------------")
		for key, value := range reqHeaders {
			log.Print(key, ":", value[0])
		}
		log.Println("----------- Request Body -----------------")
		log.Println(reqBody)

		log.Println("----------- Response Headers -----------------")
		for key, value := range resHeaders {
			log.Print(key, ":", value[0])
		}
		log.Println("----------- Response Body -----------------")
		log.Println(respBody)
		fmt.Println(respBody)
	}

}
