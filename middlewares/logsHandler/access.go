package logsHandler

import (
	"time"

	"github.com/labstack/echo/v4/middleware"
	rotatelogs "github.com/lestrrat-go/file-rotatelogs"
)

func AccessLogConfig() middleware.LoggerConfig {
	pathToAccessLog := "logs/access_log%Y%m%d%H%M%S.log"

	w, err := rotatelogs.New(
		pathToAccessLog,
		rotatelogs.WithMaxAge(time.Hour*24*5),
		rotatelogs.WithRotationTime(time.Hour*24),
	)

	if err != nil {
		panic(err)
	}

	config := middleware.LoggerConfig{
		Format: "date=${time_rfc3339}, host=${host}, method=${method}, status=${status}, uri=${uri}\n",
		Output: w,
	}

	return config
}
