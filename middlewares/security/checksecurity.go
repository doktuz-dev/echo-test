package security

import (
	"echo-doktuz-middleware/database"
	"fmt"

	"github.com/labstack/echo/v4"
)

func CheckSecurity(SessionsCollection *database.SessionsCollection) echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			if err := next(c); err != nil {
				c.Error(err)
			}
			fmt.Println("Check Security middleware")
			return nil
		}

		// return func(ctx echo.Context) error {
		// 	fmt.Println("Security middleware")
		// 	return nil
		/* jwtString := ctx.GetCookie("ApiTokenV1")
		if len(jwtString) > 0 {
			jwtToken, _ := jwt.ParseJwtString(jwtString)
			if jwtToken.Valid {
				ctx.Next()
			} else {
				ssid := jwt.ExtractSSIDfromToken(jwtToken)
				sessionEnabled := SessionsCollection.ValidateSession(nil, ssid)
				if sessionEnabled {
					jwtRefreshed, err := jwt.RefreshJwtString(ssid)
					if err != nil {
						json := mdlErrors.CreateJsonError(err)
						ctx.StopWithJSON(iris.StatusInvalidToken, json)
					}
					cookie := cookies.CreateCookie("ApiTokenV1", jwtRefreshed, time.Now().Add(10*time.Minute))
					err = SessionsCollection.UpdateRefreshedDate(nil, ssid)
					if err != nil {
						json := mdlErrors.CreateJsonError(err)
						ctx.RemoveCookie("ApiTokenV1")
						ctx.StopWithJSON(iris.StatusInternalServerError, json)
					}
					ctx.SetCookie(cookie)
					ctx.Next()
				} else {
					ctx.RemoveCookie("ApiTokenV1")
					json := iris.Map{"error": "Your credentials are not valid anymore."}
					ctx.StopWithJSON(iris.StatusUnauthorized, json)
				}
			}
		} else {
			json := iris.Map{"error": "You don't have permissions."}
			ctx.StopWithJSON(iris.StatusForbidden, json)
		} */

	}
}
