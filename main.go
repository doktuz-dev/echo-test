package main

import (
	"echo-doktuz-middleware/config"
	"echo-doktuz-middleware/database"
	_ "echo-doktuz-middleware/docs"
	"echo-doktuz-middleware/middlewares/integrations/apmpack"
	"echo-doktuz-middleware/middlewares/logsHandler"
	"echo-doktuz-middleware/repository"
	"echo-doktuz-middleware/services/monday"
	"echo-doktuz-middleware/usecase"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	echo "github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/machinebox/graphql"
	"github.com/spf13/viper"
	echoSwagger "github.com/swaggo/echo-swagger"
	"go.elastic.co/apm"
	"go.elastic.co/apm/module/apmechov4"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
)

// CreateClient creates a graphql client (safe to share across requests)
func CreateClient() *graphql.Client {
	return graphql.NewClient("https://api.monday.com/v2/")
}

var (
	googleOauthConfig = &oauth2.Config{
		RedirectURL:  "https://localhost:3000/callback",
		ClientID:     "174953318787-6g902aebs3e0nu93ek06653eilbvtdkk.apps.googleusercontent.com",
		ClientSecret: "ErwuDAFQP6FOqcNMFp-EIIGN",
		Scopes:       []string{"https://www.googleapis.com/auth/userinfo.email"},
		Endpoint:     google.Endpoint,
	}
	randomState = "random"
)

func initApm() {
	// apm configuration
	apmConfig := &apmpack.ApmConfig{}
	apmConfig.GetApmConfig()
	// transporter to send data to apm server
	transporter := apmpack.CreateTransporter(apmConfig)
	// Create Tracer with the transporter has just created
	apmTracer := &apmpack.ApmTracer{Config: apmConfig, Transporter: transporter}
	tracer := apmTracer.GetCustomTracer()
	// Set tracer like the default tracer
	apm.DefaultTracer = tracer
	apm.DefaultTracer.SetCaptureBody(apm.CaptureBodyAll)
}

// @title Echo Doktuz Middleware
// @version 1.0
// @description This is dokrs doktuz middleware written in golang.
// @termsOfService https://www.doktuz.com/terminos-y-condiciones/

// @contact.name API Support
// @contact.url https://www.doktuz.com
// @contact.email contacto@doktuz.com

// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html

// @host localhost:3000
// @schemes https
func main() {
	// app configuration
	config.SetAppEnvironment()
	initApm()
	// database
	dbConfig := database.DbConfig{}
	database.DokrDB = dbConfig.InitDatabase()
	defer dbConfig.DisconnectDB()

	// Echo instance
	e := echo.New()

	// Middleware
	e.Use(apmechov4.Middleware(apmechov4.WithTracer(apm.DefaultTracer)))
	e.Use(middleware.CORSWithConfig(middleware.DefaultCORSConfig))
	// Logs
	e.Use(middleware.LoggerWithConfig(logsHandler.AccessLogConfig()))
	// e.Use(middleware.BodyDump(logsHandler.RequestsLogs()))

	// Swagger
	e.GET("/swagger/*", echoSwagger.WrapHandler)

	// Oauth2
	e.GET("/home", handleHome)
	e.GET("/oauthLogin", handleOauth)
	e.GET("/callback", handleCallback)
	// Routes
	group := e.Group("/monday")
	// Monday router
	timeoutContext := time.Duration(viper.GetInt("context.timeout")) * time.Second
	mondayRepo := repository.NewMondayRepository(CreateClient())

	mondayUcase := usecase.NewMondayUsecase(mondayRepo, timeoutContext)
	monday.NewMondayHandler(group, mondayUcase)

	// Start server
	e.Logger.Fatal(e.StartTLS(":9090", "Certificate.crt", "Private.key"))

}

func handleHome(e echo.Context) error {
	var html = `<html><body><a href="/oauthLogin">Google Log In</a></body></html>`
	_, err := fmt.Fprintf(e.Response().Writer, html)
	return err
}

func handleOauth(e echo.Context) error {
	url := googleOauthConfig.AuthCodeURL(randomState)
	return e.Redirect(307, url)
}

func handleCallback(e echo.Context) error {
	if e.Request().FormValue("state") != randomState {
		fmt.Println("state is not valid")
		return e.Redirect(307, "/home")
	}
	token, err := googleOauthConfig.Exchange(oauth2.NoContext, e.Request().FormValue("code"))
	if err != nil {
		fmt.Printf("Could not get token: %s\n", err.Error())
		return e.Redirect(307, "/home")
	}

	resp, err := http.Get("https://www.googleapis.com/oauth2/v2/userinfo?access_token=" + token.AccessToken)
	if err != nil {
		fmt.Printf("Could not create get request: %s\n", err.Error())
		return e.Redirect(307, "/home")
	}

	defer resp.Body.Close()
	content, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Printf("Could not parse response: %s\n", err.Error())
		return e.Redirect(307, "/home")
	}

	_, erro := fmt.Fprintf(e.Response().Writer, "Response: %s", content)
	return erro

}
