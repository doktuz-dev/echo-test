package tests

import (
	"context"
	"testing"

	repo "echo-doktuz-middleware/repository"

	"echo-doktuz-middleware/models"

	"github.com/machinebox/graphql"
	"github.com/stretchr/testify/assert"
)

func TestGetAllUsers(t *testing.T) {
	conn := repo.NewMondayRepository(graphql.NewClient("https://api.monday.com/v2/"))
	response, err := conn.GetAllUsers(context.TODO())
	assert.NotEmpty(t, response)
	assert.NoError(t, err)
}

func TestGetAllColumns(t *testing.T) {
	conn := repo.NewMondayRepository(graphql.NewClient("https://api.monday.com/v2/"))
	boardId := 1092964544
	response, err := conn.GetAllColumns(context.TODO(), boardId)
	assert.NotEmpty(t, response)
	assert.NoError(t, err)
}

func TestGetAllBoards(t *testing.T) {
	conn := repo.NewMondayRepository(graphql.NewClient("https://api.monday.com/v2/"))
	response, err := conn.GetAllBoards(context.TODO())
	assert.NotEmpty(t, response)
	assert.NoError(t, err)
}

func TestGetColumnValue(t *testing.T) {
	conn := repo.NewMondayRepository(graphql.NewClient("https://api.monday.com/v2/"))
	itemId := 1094131385
	columnId := "texto"
	response, err := conn.GetColumnValue(context.TODO(), itemId, columnId)
	assert.NotEmpty(t, response)
	assert.NoError(t, err)
}

func TestChangeColumnValue(t *testing.T) {
	conn := repo.NewMondayRepository(graphql.NewClient("https://api.monday.com/v2/"))
	boardId := 1092964544
	itemId := 1094131385
	columnId := "texto"
	response, err := conn.GetColumnValue(context.TODO(), itemId, columnId)
	err = conn.ChangeColumnValue(context.TODO(), boardId, itemId, columnId, response)
	assert.NotEmpty(t, response)
	assert.NoError(t, err)
}

func TestAddTrigger(t *testing.T) {
	conn := repo.NewMondayRepository(graphql.NewClient("https://api.monday.com/v2/"))

	mond := &models.Monday{
		Payload: models.Payload{
			InputFields: models.Fields{
				BoardId: 1092964544,
				ItemId:  1094131385,
				UserId:  123234,
			},
		},
	}
	err := conn.AddTrigger(context.TODO(), mond)

	assert.NoError(t, err)
}

func TestUpdateTrigger(t *testing.T) {
	conn := repo.NewMondayRepository(graphql.NewClient("https://api.monday.com/v2/"))

	mond := &models.Monday{
		Payload: models.Payload{
			InputFields: models.Fields{
				BoardId: 1092964544,
				ItemId:  1094131385,
				ColumnValue: models.Column{
					Value: "Aloha",
				},
				PreviousColumnValue: models.Column{
					Value: "Aloha 2.0",
				},
			},
		},
	}
	err := conn.UpdateTrigger(context.TODO(), mond)
	assert.NoError(t, err)
}

func TestDeleteTrigger(t *testing.T) {
	conn := repo.NewMondayRepository(graphql.NewClient("https://api.monday.com/v2/"))
	itemId := 1094139176
	err := conn.DeleteTrigger(context.TODO(), itemId)
	assert.NoError(t, err)
}
