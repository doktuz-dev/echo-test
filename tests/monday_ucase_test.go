package tests

import (
	"context"
	"errors"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"

	"echo-doktuz-middleware/mocks"
	models "echo-doktuz-middleware/models"
	ucase "echo-doktuz-middleware/usecase"
)

func TestUseCaseGetAllUsers(t *testing.T) {
	mockMondayRepo := new(mocks.Repository)
	mockMonday := &models.User{
		Id:    123,
		Name:  "Takeshi",
		Email: "takeshihiga1998@gmail.com",
	}

	mockListMonday := make([]*models.User, 0)
	mockListMonday = append(mockListMonday, mockMonday)

	t.Run("success", func(t *testing.T) {
		mockMondayRepo.On("GetAllUsers", mock.Anything).Return(mockListMonday, nil).Once()
		u := ucase.NewMondayUsecase(mockMondayRepo, time.Second*2)
		list, err := u.GetAllUsers(context.TODO())
		assert.NotEmpty(t, list)
		assert.NoError(t, err)

		mockMondayRepo.AssertExpectations(t)
		mockMondayRepo.AssertExpectations(t)
	})

	t.Run("error-failed", func(t *testing.T) {
		mockMondayRepo.On("GetAllUsers", mock.Anything).Return(nil, errors.New("Unexpexted Error")).Once()
		u := ucase.NewMondayUsecase(mockMondayRepo, time.Second*2)
		list, err := u.GetAllUsers(context.TODO())

		assert.Empty(t, list)
		assert.Error(t, err)
		mockMondayRepo.AssertExpectations(t)
		mockMondayRepo.AssertExpectations(t)
	})
}

func TestUseCaseGetAllColumns(t *testing.T) {
	mockMondayRepo := new(mocks.Repository)
	mockMonday := &models.Column{
		Id:    "123",
		Title: "asd",
		Value: "Aloha",
	}

	mockListMonday := make([]*models.Column, 0)
	mockListMonday = append(mockListMonday, mockMonday)

	t.Run("success", func(t *testing.T) {
		mockMondayRepo.On("GetAllColumns", mock.Anything, mock.AnythingOfType("int")).Return(mockListMonday, nil).Once()
		u := ucase.NewMondayUsecase(mockMondayRepo, time.Second*2)
		boardId := 1092964544
		list, err := u.GetAllColumns(context.TODO(), boardId)
		assert.NotEmpty(t, list)
		assert.NoError(t, err)

		mockMondayRepo.AssertExpectations(t)
		mockMondayRepo.AssertExpectations(t)
	})

	t.Run("error-failed", func(t *testing.T) {
		mockMondayRepo.On("GetAllColumns", mock.Anything, mock.AnythingOfType("int")).Return(nil, errors.New("Unexpexted Error")).Once()
		u := ucase.NewMondayUsecase(mockMondayRepo, time.Second*2)
		boardId := 1092964544
		list, err := u.GetAllColumns(context.TODO(), boardId)

		assert.Empty(t, list)
		assert.Error(t, err)
		mockMondayRepo.AssertExpectations(t)
		mockMondayRepo.AssertExpectations(t)
	})
}

func TestUseCaseGetAllBoards(t *testing.T) {
	mockMondayRepo := new(mocks.Repository)
	mockMonday := &models.Board{
		Id:   "123",
		Name: "prueba",
	}

	mockListMonday := make([]*models.Board, 0)
	mockListMonday = append(mockListMonday, mockMonday)

	t.Run("success", func(t *testing.T) {
		mockMondayRepo.On("GetAllBoards", mock.Anything).Return(mockListMonday, nil).Once()
		u := ucase.NewMondayUsecase(mockMondayRepo, time.Second*2)
		list, err := u.GetAllBoards(context.TODO())
		assert.NotEmpty(t, list)
		assert.NoError(t, err)

		mockMondayRepo.AssertExpectations(t)
		mockMondayRepo.AssertExpectations(t)
	})

	t.Run("error-failed", func(t *testing.T) {
		mockMondayRepo.On("GetAllBoards", mock.Anything).Return(nil, errors.New("Unexpexted Error")).Once()
		u := ucase.NewMondayUsecase(mockMondayRepo, time.Second*2)
		list, err := u.GetAllBoards(context.TODO())

		assert.Empty(t, list)
		assert.Error(t, err)
		mockMondayRepo.AssertExpectations(t)
		mockMondayRepo.AssertExpectations(t)
	})
}

func TestUseCaseChangeColumnValue(t *testing.T) {
	mockMondayRepo := new(mocks.Repository)
	mockMonday := "Test"

	t.Run("success", func(t *testing.T) {
		mockMondayRepo.On(
			"GetColumnValue",
			mock.Anything,
			mock.AnythingOfType("int"),
			mock.AnythingOfType("string"),
		).Return(mockMonday, nil).Once()

		mockMondayRepo.On(
			"ChangeColumnValue",
			mock.Anything,
			mock.AnythingOfType("int"),
			mock.AnythingOfType("int"),
			mock.AnythingOfType("string"),
			mock.AnythingOfType("string"),
		).Return(nil).Once()
		u := ucase.NewMondayUsecase(mockMondayRepo, time.Second*2)
		mond := &models.Monday{
			Payload: models.Payload{
				InputFields: models.Fields{
					BoardId:        1092964544,
					ItemId:         1094130652,
					SourceColumnId: "texto",
					TargetColumnId: "texto",
				},
			},
		}
		err := u.ChangeColumnValue(context.TODO(), mond)
		assert.NoError(t, err)

		mockMondayRepo.AssertExpectations(t)
		mockMondayRepo.AssertExpectations(t)
	})

	t.Run("error-failed", func(t *testing.T) {
		mockMondayRepo.On(
			"GetColumnValue",
			mock.Anything,
			mock.AnythingOfType("int"),
			mock.AnythingOfType("string"),
		).Return(mockMonday, nil).Once()

		mockMondayRepo.On(
			"ChangeColumnValue",
			mock.Anything,
			mock.AnythingOfType("int"),
			mock.AnythingOfType("int"),
			mock.AnythingOfType("string"),
			mock.AnythingOfType("string"),
		).Return(models.ErrNotFound).Once()
		u := ucase.NewMondayUsecase(mockMondayRepo, time.Second*2)
		mond := &models.Monday{
			Payload: models.Payload{
				InputFields: models.Fields{
					BoardId:        1092964544,
					ItemId:         1094130652,
					SourceColumnId: "texto",
					TargetColumnId: "texto0",
				},
			},
		}
		err := u.ChangeColumnValue(context.TODO(), mond)

		assert.Error(t, err)
		mockMondayRepo.AssertExpectations(t)
		mockMondayRepo.AssertExpectations(t)
	})
}

func TestUseCaseAddTrigger(t *testing.T) {
	mockMondayRepo := new(mocks.Repository)

	t.Run("success", func(t *testing.T) {
		mockMondayRepo.On(
			"AddTrigger",
			mock.Anything,
			mock.AnythingOfType("*models.Monday"),
		).Return(nil).Once()
		u := ucase.NewMondayUsecase(mockMondayRepo, time.Second*2)
		mond := &models.Monday{
			Payload: models.Payload{
				InputFields: models.Fields{
					BoardId: 1092964544,
					ItemId:  1094131385,
					UserId:  123234,
				},
			},
		}
		err := u.AddTrigger(context.TODO(), mond)
		assert.NoError(t, err)

		mockMondayRepo.AssertExpectations(t)
		mockMondayRepo.AssertExpectations(t)
	})

	t.Run("error-failed", func(t *testing.T) {

		mockMondayRepo.On(
			"AddTrigger",
			mock.Anything,
			mock.AnythingOfType("*models.Monday"),
		).Return(errors.New("Unexpexted Error")).Once()
		u := ucase.NewMondayUsecase(mockMondayRepo, time.Second*2)
		mond := &models.Monday{
			Payload: models.Payload{
				InputFields: models.Fields{
					BoardId: 1092964544,
					ItemId:  1094131385,
					UserId:  123234,
				},
			},
		}
		err := u.AddTrigger(context.TODO(), mond)

		assert.Error(t, err)
		mockMondayRepo.AssertExpectations(t)
		mockMondayRepo.AssertExpectations(t)
	})
}

func TestUseCaseUpdateTrigger(t *testing.T) {
	mockMondayRepo := new(mocks.Repository)

	t.Run("success", func(t *testing.T) {
		mockMondayRepo.On(
			"UpdateTrigger",
			mock.Anything,
			mock.AnythingOfType("*models.Monday"),
		).Return(nil).Once()
		u := ucase.NewMondayUsecase(mockMondayRepo, time.Second*2)

		mond := &models.Monday{
			Payload: models.Payload{
				InputFields: models.Fields{
					BoardId: 1092964544,
					ItemId:  1094131385,
					ColumnValue: models.Column{
						Value: "Aloha",
					},
					PreviousColumnValue: models.Column{
						Value: "Aloha 2.0",
					},
				},
			},
		}
		err := u.UpdateTrigger(context.TODO(), mond)
		assert.NoError(t, err)

		mockMondayRepo.AssertExpectations(t)
		mockMondayRepo.AssertExpectations(t)
	})

	t.Run("error-failed", func(t *testing.T) {

		mockMondayRepo.On(
			"UpdateTrigger",
			mock.Anything,
			mock.AnythingOfType("*models.Monday"),
		).Return(errors.New("Unexpexted Error")).Once()
		u := ucase.NewMondayUsecase(mockMondayRepo, time.Second*2)

		mond := &models.Monday{
			Payload: models.Payload{
				InputFields: models.Fields{
					BoardId: 1092964544,
					ItemId:  1094131385,
					ColumnValue: models.Column{
						Value: "Aloha",
					},
					PreviousColumnValue: models.Column{
						Value: "Aloha 2.0",
					},
				},
			},
		}
		err := u.UpdateTrigger(context.TODO(), mond)

		assert.Error(t, err)
		mockMondayRepo.AssertExpectations(t)
		mockMondayRepo.AssertExpectations(t)
	})
}

func TestUseCaseDeleteTrigger(t *testing.T) {
	mockMondayRepo := new(mocks.Repository)

	t.Run("success", func(t *testing.T) {
		mockMondayRepo.On(
			"DeleteTrigger",
			mock.Anything,
			mock.AnythingOfType("int"),
		).Return(nil).Once()
		u := ucase.NewMondayUsecase(mockMondayRepo, time.Second*2)

		mond := &models.Monday{
			Payload: models.Payload{
				InputFields: models.Fields{
					BoardId: 1092964544,
					ItemId:  1094131385,
				},
			},
		}
		err := u.DeleteTrigger(context.TODO(), mond)
		assert.NoError(t, err)

		mockMondayRepo.AssertExpectations(t)
		mockMondayRepo.AssertExpectations(t)
	})

	t.Run("error-failed", func(t *testing.T) {

		mockMondayRepo.On(
			"DeleteTrigger",
			mock.Anything,
			mock.AnythingOfType("int"),
		).Return(errors.New("Unexpexted Error")).Once()
		u := ucase.NewMondayUsecase(mockMondayRepo, time.Second*2)

		mond := &models.Monday{
			Payload: models.Payload{
				InputFields: models.Fields{
					BoardId: 1092964544,
					ItemId:  1094131385,
				},
			},
		}
		err := u.DeleteTrigger(context.TODO(), mond)

		assert.Error(t, err)
		mockMondayRepo.AssertExpectations(t)
		mockMondayRepo.AssertExpectations(t)
	})
}
