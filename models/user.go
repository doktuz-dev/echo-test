package models

import "go.mongodb.org/mongo-driver/bson/primitive"

type LoginUser struct {
	ID       primitive.ObjectID `json:"_id" bson:"_id"`
	Username string             `json:"username"`
	Password string             `json:"password"`
	Roles    []Roles            `json:"roles"`
}
