package models

import "github.com/machinebox/graphql"

type Monday struct {
	Payload Payload `json:"payload"`
}

type Payload struct {
	InputFields Fields `json:"inputFields"`
}

type Fields struct {
	BoardId             int    `json:"boardId,omitempty"`
	ItemId              int    `json:"itemId,omitempty"`
	SourceColumnId      string `json:"sourceColumnId,omitempty"`
	TargetColumnId      string `json:"targetColumnId,omitempty"`
	UserId              int    `json:"userId,omitempty"`
	ColumnValue         Column `json:"columnValue,omitempty"`
	PreviousColumnValue Column `json:"previousColumnValue,omitempty"`
}

type MondayClient struct {
	Client *graphql.Client
}

type User struct {
	Id    int    `json:"id,omitempty"`
	Name  string `json:"name,omitempty"`
	Email string `json:"email,omitempty"`
}

type Column struct {
	Id       string `json:"id,omitempty"`
	Title    string `json:"title,omitempty"`
	Value    string `json:"value,omitempty"`
	Type     string `json:"type,omitempty"`
	Settings string `json:"settings_str,omitempty"`
}

type Board struct {
	Id   string `json:"id,omitempty"`
	Name string `json:"name,omitempty"`
}

type ResponseSuccess struct {
	Message string `json:"message,omitempty"`
}
