package models

type Session struct {
	ID              string `json:"_id" bson:"_id"`
	Ssid            string `json:"ssid"`
	Username        string `json:"username"`
	Enabled         bool   `json:"enabled"`
	StartDate       int64  `json:"startDate"`
	EndDate         int64  `json:"endDate"`
	App             string `json:"app"`
	CreatedDate     int64  `json:"createdDate"`
	LastUpdatedDate int64  `json:"lastUpdatedDate"`
}
