package database

import (
	"context"
	"fmt"
	"time"

	"github.com/spf13/viper"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var DokrDB *mongo.Database

type DbConfig struct {
	Ctx    context.Context
	Client *mongo.Client
	Cancel context.CancelFunc
}

func (config *DbConfig) InitDatabase() *mongo.Database {
	uri := viper.GetString("database.uri")
	name := viper.GetString("database.name")
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(uri))
	if err != nil {
		fmt.Println("InitDatabase", err)
		panic(err)
	}
	config.Ctx = ctx
	config.Client = client
	config.Cancel = cancel
	return client.Database(name)
}

func (config *DbConfig) DisconnectDB() {
	err := config.Client.Disconnect(config.Ctx)
	if err != nil {
		fmt.Println("DisconnectDB", err)
		panic(err)
	}
}
