package database

import (
	"context"
	"echo-doktuz-middleware/models"
	"fmt"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type SessionsCollection struct {
	C *mongo.Collection
}

type SessionsStore interface {
	AddSession()
}

func (s *SessionsCollection) AddSession(ctx context.Context, session *models.Session) error {
	if len(session.ID) == 0 {
		session.ID = primitive.NewObjectID().Hex()
	}
	res, err := s.C.InsertOne(ctx, session)
	if err != nil {
		fmt.Println("AddSession", err)
		return err
	}
	fmt.Println("AddSession", res)
	return nil
}

// If returns true it means the session id exists and it's enabled.
func (s *SessionsCollection) ValidateSession(ctx context.Context, ssid string) bool {
	filter := bson.M{"ssid": ssid, "enabled": true}
	res := s.C.FindOne(ctx, filter)
	if res.Err() == mongo.ErrNoDocuments {
		return false
	}
	return true
}

// Update lastUpdateDate field in the session.
func (s *SessionsCollection) UpdateRefreshedDate(ctx context.Context, ssid string) error {
	filter := bson.M{"ssid": ssid}
	update := bson.M{"$set": bson.M{"lastupdateddate": time.Now().Unix()}}
	res, err := s.C.UpdateOne(ctx, filter, update)
	if err != nil {
		return err
	}
	if res.MatchedCount > 0 {
		return nil
	} else {
		return mongo.ErrNoDocuments
	}

}
