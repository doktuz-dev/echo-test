package database

import (
	"context"
	"echo-doktuz-middleware/models"
	"fmt"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type LoginCollection struct {
	C *mongo.Collection
}

type LoginStore interface {
	ValidateUser()
}

func (s *LoginCollection) ValidateUser(ctx context.Context, user models.LoginUser) bool {
	filter := bson.M{"username": user.Username}
	res := s.C.FindOne(ctx, filter)
	if res.Err() != nil {
		fmt.Println("ValidateUser", res.Err())
		return false
	}
	return true
}
