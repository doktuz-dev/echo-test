# Builder
FROM golang:1.15.10-alpine3.13 as builder

RUN apk update && apk upgrade && \
    apk --update add git make

WORKDIR /echo-doktuz-middleware

COPY . .

RUN make engine

# Distribution
FROM alpine:latest

RUN apk update && apk upgrade && \
    apk --update --no-cache add tzdata && \
    mkdir /app 

WORKDIR /app 

EXPOSE 9090

COPY --from=builder /app/engine /app

CMD /app/engine
