package usecase

import (
	"context"
	"echo-doktuz-middleware/models"
	"echo-doktuz-middleware/services/monday"
	"strings"
	"time"
)

type mondayUsecase struct {
	mondayRepo     monday.Repository
	contextTimeout time.Duration
}

// NewArticleUsecase will create new an articleUsecase object representation of article.Usecase interface
func NewMondayUsecase(mr monday.Repository, timeout time.Duration) monday.Usecase {
	return &mondayUsecase{
		mondayRepo:     mr,
		contextTimeout: timeout,
	}
}

func (a *mondayUsecase) GetAllUsers(c context.Context) ([]*models.User, error) {
	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	users, err := a.mondayRepo.GetAllUsers(ctx)

	if err != nil {
		return nil, err
	}
	return users, nil
}

func (a *mondayUsecase) GetAllBoards(c context.Context) ([]*models.Board, error) {
	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	boards, err := a.mondayRepo.GetAllBoards(ctx)

	if err != nil {
		return nil, err
	}
	return boards, nil
}

func (a *mondayUsecase) GetAllColumns(c context.Context, id int) ([]*models.Column, error) {
	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	colums, err := a.mondayRepo.GetAllColumns(ctx, id)

	if err != nil {
		return nil, err
	}
	return colums, nil
}

func (a *mondayUsecase) ChangeColumnValue(c context.Context, modMonday *models.Monday) error {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	boardId := modMonday.Payload.InputFields.BoardId
	itemId := modMonday.Payload.InputFields.ItemId
	sourceColumnId := modMonday.Payload.InputFields.SourceColumnId
	targetColumnId := modMonday.Payload.InputFields.TargetColumnId

	value, erro := a.mondayRepo.GetColumnValue(ctx, itemId, sourceColumnId)
	if erro != nil {
		return erro
	}
	upperCase := strings.ToUpper(value)
	erro = a.mondayRepo.ChangeColumnValue(ctx, boardId, itemId, targetColumnId, upperCase)
	if erro != nil {
		return erro
	}

	return nil
}

func (a *mondayUsecase) AddTrigger(c context.Context, modMonday *models.Monday) error {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err := a.mondayRepo.AddTrigger(ctx, modMonday)
	if err != nil {
		return err
	}

	return nil
}

func (a *mondayUsecase) UpdateTrigger(c context.Context, modMonday *models.Monday) error {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()

	err := a.mondayRepo.UpdateTrigger(ctx, modMonday)
	if err != nil {
		return err
	}

	return nil
}

func (a *mondayUsecase) DeleteTrigger(c context.Context, modMonday *models.Monday) error {

	ctx, cancel := context.WithTimeout(c, a.contextTimeout)
	defer cancel()
	itemId := modMonday.Payload.InputFields.ItemId

	err := a.mondayRepo.DeleteTrigger(ctx, itemId)
	if err != nil {
		return err
	}

	return nil
}
